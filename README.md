## 免翻墙镜像
本页面收集了墙内镜像。请将此页加为书签。若镜像被封锁，请访问本页面获取最新镜像。
* Google 搜索：https://s3-ap-southeast-1.amazonaws.com/google.cn/index.html
* Google 搜索：https://s3.amazonaws.com/google./index.html    
* 自由微博：https://s3-ap-southeast-1.amazonaws.com/freeweibo2/index.html
* 自由微博：https://s3.amazonaws.com/freeweibo./index.html
* 中国数字时代：https://s3-ap-southeast-1.amazonaws.com/cdtimes2/index.html
* 中国数字时代：https://s3.amazonaws.com/cdtimes./index.html
* 泡泡（未经审查的网络报道）：https://s3-ap-southeast-1.amazonaws.com/pao-pao2/index.html
* 泡泡（未经审查的网络报道）：https://s3.amazonaws.com/pao-pao/index.html
* 蓝灯(Lantern)以及自由微博和GreatFire.org官方中文论坛：https://lanternforum.greatfire.org

<img src="https://raw.githubusercontent.com/greatfire/z/master/logos.gif" />

## 新闻
@evacide FYI we just unblocked Google search in China - details about our anti-censorship hack here: <a href="http://us7.campaign-archive2.com/?u=854fca58782082e0cbdf204a0&id=6f3dda1ae7&e=326c493581">us7.campaign-archive2.com/</a> (2014年06月02日 19:16)
 ---
RT @McAndrew: Hell of a great hack against censorship. RT @GreatFireChina: We've just unblocked Google search in China <a href="https://t.co/gCKPDZQ">t.co/gCKPDZQ</a>… (2014年06月02日 18:57)
 ---
RT @pabischoff: 2 days before ‘May 35th’, @GreatFireChina “unblocks” Google search in China <a href="http://www.techinasia.com/2-days-35th-greatfireorg-unblocks-google-search-china/">www.techinasia.com/2-days-35th-greatfireorg-unblocks-google-search-china/</a> via @Techinasia (2014年06月02日 18:10)
 ---
@pabischoff @Techinasia once the url is shared, it is unblockable and our @FreeWeibo mirror is now indexed on Google - in time, will spread (2014年06月02日 18:08)
 ---
We've just unblocked Google search in China <a href="https://s3-ap-southeast-1.amazonaws.com/google.cn/index.html">s3-ap-southeast-1.amazonaws.com/google.cn/index.html</a> (2014年06月02日 17:04)
 ---
免翻墙镜像收录，不定期更新 <a href="https://github.com/greatfire/wiki">github.com/greatfire/wiki</a> (2014年06月02日 16:29)
 ---
RT @tomphillipsin: "To claim Chinese are unsuited to or not ready for democracy &amp; freedom is to view them as less than human beings" http:/… (2014年06月02日 14:34)
 ---
@niubi @mrbaopanrui Apps might not use the same IP. Please try accessing <a href="http://www.google.com.hk">www.google.com.hk</a> on mobile or desktop. (2014年06月02日 12:16)
 ---
.@chengr28 GFW连Google80端口的明文流量也封锁了。 只封GoAgent没这个必要吧。 (2014年06月02日 05:56)
 ---
天安门事件前夕谷歌被封锁; 镜像网站提供不受审查的访问 <a href="https://zh.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access">zh.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access</a> (2014年06月02日 05:44)
 ---
[Breaking New]All google products are now blocked in China. <a href="https://en.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access">en.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access</a> (2014年06月02日 05:30)
 ---
Google severely disrupted prior to Tiananmen Anniversary; Mirror sites enable uncensored access to information <a href="https://en.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access">en.greatfire.org/blog/2014/jun/google-disrupted-prior-tiananmen-anniversary-mirror-sites-enable-uncensored-access</a> (2014年06月02日 05:20)
 ---
RT @yancaiwm: 转通知：鉴于招远事件震惊天下，各网评员在引导舆论时一定要注意技巧，1要把矛头指向围观者，站在道德高度谴责他们麻木冷血，从而减轻政府的责任和压力，2由知名网评员把这股祸水引向宗教，大家随后跟进一箭双雕，只要贴上邪教的标签，就能淡化凶手和官方的利益纠葛，… (2014年06月01日 15:32)
 ---
新疆通信管制五周年记 <a href="http://blog.5istar.net/archives/587">blog.5istar.net/archives/587</a> (2014年06月01日 05:52)
 ---
RT @chengr28: 好吧看来我又错了……刚花了几十分钟观察GFW对Google的TCP协议封锁，应该是这样的：以10分钟为一个周期，前3分钟通讯通畅，后7分钟丢弃所有TCP协议数据包，如此循环。也就是说像 0:00 - 0:03 可以用，0:04 - 0:10 不能用…… (2014年06月01日 03:42)
 ---
RT @hancocktom: Government of Kashgar in Xinjiang denies it released a statement saying it would cut off the internet <a href="http://news.sina.com.cn/c/2014-05-28/131130248764.shtml?bsh_bid=416061313&utm_content=buffer88b09&utm_medium=social&utm_source=twitter.com&utm_campaign=buffer">news.sina.com.cn/c/2014-05-28/131130248764.shtml</a> (2014年05月30日 06:57)
 ---
RT @wenyunchao: Remembering Tiananmen: The lessons of history | The Economist <a href="http://www.economist.com/news/china/21603016-our-bureau-chief-leaves-china-he-reflects-crushing-protests-he-witnessed-25-years">www.economist.com/news/china/21603016-our-bureau-chief-leaves-china-he-reflects-crushing-protests-he-witnessed-25-years</a> (2014年05月30日 04:56)
 ---
@prchovanec thanks for sharing all of these amazing photos (2014年05月30日 04:55)
 ---
RT @prchovanec: May 29, 1989 - Demonstrators march in Tiananmen Square. Banner reads "History on Trial" <a href="https://twitter.com/prchovanec/status/472116755629105152/photo/1">twitter.com/prchovanec/status/472116755629105152/photo/1</a> (2014年05月30日 04:54)
 ---
not about censorship but important: Your Princess Is in Another Castle: Misogyny, Entitlement, and Nerds <a href="http://www.thedailybeast.com/articles/2014/05/27/your-princess-is-in-another-castle-misogyny-entitlement-and-nerds.html">www.thedailybeast.com/articles/2014/05/27/your-princess-is-in-another-castle-misogyny-entitlement-and-nerds.html</a> (2014年05月29日 23:37)
 ---
